import auth from './auth/routes'
import home from './home/routes'
import timeline from './timeline/routes'
import errors from './errors/routes'
import orders from './orders/routes'

export default[...home, ...auth, ...timeline, ...orders, ...errors]
