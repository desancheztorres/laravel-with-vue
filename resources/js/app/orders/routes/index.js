import { Order } from '../components'

export default [
    {
        path: '/orders',
        component: Order,
        name: 'orders',
        meta: {
            needsAuth: true,
        }
    }
]
