import Vue from 'vue'

export const Order = Vue.component('order', require('./Order').default)
export const Table = Vue.component('order-table', require('./OrderTable').default)
