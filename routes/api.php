<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/register', 'Auth\Authcontroller@register');
Route::post('/login', 'Auth\Authcontroller@login');
Route::post('/logout', 'Auth\Authcontroller@logout');


Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/me', 'Auth\Authcontroller@user');
    Route::get('/timeline', 'Timelinecontroller@index');
});
